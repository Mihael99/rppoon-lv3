﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;

        private Logger()
        {
            this.filePath = "E:\\newfile.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {   
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath,true))
            {
                writer.WriteLine(message);
            }
        }
        public void setFilePath(string file)
        {
            this.filePath = file;
        }
    }
}
