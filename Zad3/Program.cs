﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.GetInstance();
            logger.setFilePath("E:\\LV3\\logger1.txt");
            logger.Log("Prvo logiranje.");
            logger.Log("Drugo logiranje.");
            // Ako  je  datoteka  postavljenana jednom mjestu u tekstu programa, hoće li uporaba  loggera  
            //na drugim mjesta u testu programa pisati u istu datoteku (pretpostavka je kako nisu ponovo postavljene)? Hoće.
        }
    }
}
