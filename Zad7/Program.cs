﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad7
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification Notification = new ConsoleNotification("Mihael", "Zadatak 7.", "Klasu ConsoleNotificationiz primjera 3 izmijeniti tako da ugrađuje sučelje Protoypeiz primjera 1.2. Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja? ", DateTime.Now, Category.INFO, ConsoleColor.Green);
            Notification.Clone();
            //Odgovor: U ovom slučaju nema razlike u funkciji izmedju plitkog i dubokog kopiranja jer objekt i da duboko kopiramo ne možemo mijenjati.
            //Razlike se ocituje jedino u memoriji.
        }
    }
}
