﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, m;
            RandomMatrix Matrix1 = RandomMatrix.GetInstance();
            m = 4;
            n = 6;
            double[][] array2d;
            array2d = Matrix1.MatrixGenerator(m, n);
            for(int i = 0; i < m; i++)
            {
                Console.WriteLine("Red[" + i + "]:");
                for(int j = 0; j < n; j++)
                {
                    Console.WriteLine(array2d[i][j]);
                }
            }
        }
    }
}
