﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class RandomMatrix
    {
        private static RandomMatrix instance;
        private Random generator;

        private RandomMatrix() 
        {
            this.generator = new Random();
        }
        public static RandomMatrix GetInstance() {
            if (instance == null) { 
                instance = new RandomMatrix(); 
            } 
            return instance; 
        }
        public double[][] MatrixGenerator(int m, int n)
        {
            double[][] matrix = new double[m][];
            for (int i = 0; i < m; i++) {
                matrix[i] = new double[n]; 
                for(int j = 0; j < n; j++)
                {
                    matrix[i][j] = generator.NextDouble();
                }
            }
            return matrix;
        }
    } //Koliko odgovornosti ima navedena metoda? Navedena metoda ima više odgovornosti, stvoriti i popuniti matricu, te ju vratiti.
}
