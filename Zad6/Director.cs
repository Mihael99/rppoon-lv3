﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6
{
    class Director
    {
        public void InfoNotification(NotificationBuilder Builder, string Author)
        {
            Builder.SetAuthor(Author);
            Builder.SetColor(ConsoleColor.DarkBlue);
            Builder.SetLevel(Category.INFO);
            Builder.SetText("This is an Info Notification.");
            Builder.SetTime(DateTime.Now);
            Builder.SetTitle("INFO Notification");
        }
        public void AlertNotification(NotificationBuilder Builder, string Author)
        {
            Builder.SetAuthor(Author);
            Builder.SetColor(ConsoleColor.Yellow);
            Builder.SetLevel(Category.ALERT);
            Builder.SetText("This is an Alert Notification.");
            Builder.SetTime(DateTime.Now);
            Builder.SetTitle("ALERT Notification");
        }
        public void ErrorNotification(NotificationBuilder Builder, string Author)
        {
            Builder.SetAuthor(Author);
            Builder.SetColor(ConsoleColor.Red);
            Builder.SetLevel(Category.ERROR);
            Builder.SetText("This is an Error Notification.");
            Builder.SetTime(DateTime.Now);
            Builder.SetTitle("ERROR Notification");
        }
    }
}
