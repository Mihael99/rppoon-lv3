﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5
{
    class ConsoleNotification
    {
        private String Author { get;  set; }
        private String Title { get;  set; }
        private String Text { get;  set; }
        private DateTime Timestamp { get;  set; }
        private Category Level { get;  set; }
        private ConsoleColor Color { get;  set; }
        public ConsoleNotification(String author, String title, String text, DateTime time, Category level, ConsoleColor color)
        {
            this.Author = author;
            this.Title = title;
            this.Text = text;
            this.Timestamp = time;
            this.Level = level;
            this.Color = color;
        }
    }
}
