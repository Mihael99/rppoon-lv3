﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification FirstNotification = new ConsoleNotification("Mihael Marjanović", "LV3", "4.zadatak, provjera funckionalnosti primjera 3", DateTime.Now, Category.INFO, ConsoleColor.Blue);
            NotificationManager NoteManager = new NotificationManager();
            NoteManager.Display(FirstNotification);
            ConsoleNotification SecondNotification = new ConsoleNotification("Mihael Marjanović", "LV3", "4.zadatak, provjera funckionalnosti primjera 3", DateTime.Now, Category.ERROR, ConsoleColor.Red);
            NoteManager.Display(SecondNotification);
            ConsoleNotification ThirdNotification = new ConsoleNotification("Mihael Marjanović", "LV3", "4.zadatak, provjera funckionalnosti primjera 3", DateTime.Now, Category.ALERT, ConsoleColor.Yellow);
            NoteManager.Display(ThirdNotification);
        }
    }
}
